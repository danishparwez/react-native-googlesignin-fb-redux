import React, {Component} from 'react';
import {connect} from 'react-redux';
import Home from './home';
import Profile from './profile';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
const Stack = createStackNavigator();
class Navigation extends Component {
  state = {};
  componentDidMount() {
    console.log(this.props.isLoggedIn);
    //this.componentName = this.props.isLoggedIn === true ? 'Profile' : 'Home';
  }
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator
        //   initialRouteName={
        //     this.props.isLoggedIn === true ? 'Profile' : 'Home'
        //   }>
        >
          {this.props.isLoggedIn ? (
            <Stack.Screen name="Profile" component={Profile} />
          ) : (
            <Stack.Screen name="Home" component={Home} />
          )}
          {/* <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="Profile" component={Profile} /> */}
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLoggedIn: state.isLoggedIn,
  };
};
export default connect(
  mapStateToProps,
  null,
)(Navigation);
