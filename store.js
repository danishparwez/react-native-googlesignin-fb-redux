import {createStore, applyMiddleware} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';
//import rootReducer from './reducers/index';
import rootReducer from './reducer';
import {AsyncStorage} from 'react-native';

const persistConfig = {
  key: 'authType',
  storage: AsyncStorage,
};
const p = persistReducer(persistConfig, rootReducer);
const store = createStore(p);
const persistor = persistStore(store);
export {persistor, store};
