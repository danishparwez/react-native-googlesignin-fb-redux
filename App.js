/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Home from './home';
import Profile from './profile';
import {Provider} from 'react-redux';
const Stack = createStackNavigator();
// const name = 'Profile';
// const stored = useStore();
import Navigation from './navigation';
// const check = stored.getState().isLoggedIn;
// const componentName = check == true ? 'Profile' : 'Home';
import {persistor, store} from './store';
import {PersistGate} from 'redux-persist/es/integration/react';
class App extends React.Component {
  __isMounted = false;
  state = {
    test: 'false',
  };

  componentDidMount() {
    this.__isMounted = true;
    if (this.__isMounted) {
      setTimeout(() => {
        console.log('enterrrrr');
        SplashScreen.hide();
      }, 5000);
    }
    // this.name = 'Profile';
  }

  componentWillUnmount() {
    this.__isMounted = false;
  }
  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <Navigation />
          {/* <NavigationContainer>
          <Stack.Navigator initialRouteName={componentName}>
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="Profile" component={Profile} />
          </Stack.Navigator>
        </NavigationContainer> */}
        </PersistGate>
      </Provider>
    );
  }
}
//webclient id="12987450032-36vgtgijmsb91j70gs4o5d07i92s0v3i.apps.googleusercontent.com"
export default App;
