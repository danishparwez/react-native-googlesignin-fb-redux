import React, {Component} from 'react';
import {Text, Button, Image, View, ImageBackground} from 'react-native';
import {connect} from 'react-redux';
import {GoogleSignin} from 'react-native-google-signin';
import PushNotification from 'react-native-push-notification';
import {LoginButton, AccessToken} from 'react-native-fbsdk';
class Profile extends Component {
  state = {};
  componentDidMount() {
    // console.log('ppppppp', this.props.data);
    PushNotification.configure({
      onRegister: function(token) {
        // console.log('Notification TOKEN: Generated', token);
      },
      onNotification: notification => {
        //  console.log('NOTIFICATION:', notification);
      },
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      popInitialNotification: true,
      requestPermissions: true,
      actions: '["Accept", "Reject"]',
    });
    this.push_notification();
  }
  push_notification = () => {
    PushNotification.localNotification({
      title: 'Logged In',
      message: `${this.props.data.givenName} is Logged In`,
    });
  };
  loggedOut_push_notification = () => {
    PushNotification.localNotification({
      title: 'Logged Out',
      message: `${this.props.data.givenName} is Logged Out`,
    });
  };
  logoutFeature = async () => {
    GoogleSignin.configure({
      webClientId:
        '12987450032-36vgtgijmsb91j70gs4o5d07i92s0v3i.apps.googleusercontent.com',
    });
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      this.loggedOut_push_notification();
      this.props.logout();
    } catch (error) {
      console.error(error);
    }
  };
  fblogout = () => {
    this.loggedOut_push_notification();
    this.props.logout();
  };
  render() {
    return (
      <>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#DCCCC8',
          }}>
          <Image
            source={{uri: this.props.data.photo}}
            style={{
              width: 100,
              height: 100,
              borderRadius: 50,
              display: 'flex',
              alignItems: 'center',
            }}
          />
          <Text
            style={{
              color: 'blue',
              fontSize: 20,
              textAlign: 'center',
              marginTop: 20,
            }}>
            Welcome to {this.props.data.givenName} {this.props.data.familyName}
          </Text>
          <Text style={{color: 'blue', fontSize: 20, marginTop: 5}}>
            Email: {this.props.data.email}
          </Text>
        </View>
        {this.props.data.type === 'GS' ? (
          <Button
            style={{fontSize: 25}}
            onPress={this.logoutFeature}
            title="LogOut"
          />
        ) : (
          <LoginButton
            onLogoutFinished={() => this.fblogout()}
            style={{width: '100%', height: '5%'}}
          />
        )}
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    data: state.data,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch({type: 'logOut'}),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Profile);

// export default Profile;
