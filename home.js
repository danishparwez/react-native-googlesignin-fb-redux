import React, {Component} from 'react';
import {Text, Button, View, Alert} from 'react-native';
import {GoogleSignin, GoogleSigninButton} from 'react-native-google-signin';
import {connect} from 'react-redux';
import {LoginButton, AccessToken} from 'react-native-fbsdk';
class Home extends Component {
  state = {};
  signin = async () => {
    // GoogleSignin.configure({
    //   androidClientId:
    //     '1098583045625-l6c11t4tuvv5n6iqivs9brq4o4s5j5lh.apps.googleusercontent.com',
    // });
    GoogleSignin.configure({
      webClientId:
        '12987450032-36vgtgijmsb91j70gs4o5d07i92s0v3i.apps.googleusercontent.com',
    });
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      this.setState({userInfo});
      let obj = {
        givenName: userInfo.user.givenName,
        familyName: userInfo.user.familyName,
        photo: userInfo.user.photo,
        email: userInfo.user.email,
        type: 'GS',
      };
      this.props.islogged(obj);
    } catch (error) {
      if (Number.isInteger(parseInt(error.code))) {
        console.log('Error');
      } else {
        if (error.code === statusCodes.SIGN_IN_CANCELLED) {
          // user cancelled the login flow
        } else if (error.code === statusCodes.IN_PROGRESS) {
          // operation (f.e. sign in) is in progress already
        } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
          // play services not available or outdated
        } else {
          throw error;
          // some other error happened
        }
      }
    }
  };
  fbSignin = (error, result) => {
    console.log('reachhhhh');
    if (error) {
      console.log('login has error: ' + error);
    } else if (result.isCancelled) {
      console.log('login is cancelled.');
    } else {
      AccessToken.getCurrentAccessToken().then(data => {
        fetch(
          `https://graph.facebook.com/me?fields=id,email,name,first_name,last_name,gender,picture,cover&access_token=${data.accessToken.toString()}`,
        )
          .then(data => {
            return data.json();
          })
          .then(data => {
            let obj = {
              givenName: data.first_name,
              familyName: data.last_name,
              photo: data.picture.data.url,
              email: 'N.A',
              type: 'FB',
            };
            this.props.islogged(obj);
          });
      });
    }
  };
  render() {
    return (
      <>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#B6928A',
          }}>
          <GoogleSigninButton
            style={{width: 192, height: 48}}
            size={GoogleSigninButton.Size.Wide}
            color={GoogleSigninButton.Color.Dark}
            onPress={this.signin}
            disabled={this.state.isSigninInProgress}
          />
          <LoginButton
            readPermissions={['public_profile']}
            onLoginFinished={(error, result) => {
              console.log('doneee');
              if (error) {
                console.log('login has error: ' + error);
              } else if (result.isCancelled) {
                console.log('login is cancelled.');
              } else {
                AccessToken.getCurrentAccessToken().then(data => {
                  console.log(data);
                  fetch(
                    `https://graph.facebook.com/me?fields=id,email,name,first_name,last_name,gender,picture,cover&access_token=${data.accessToken.toString()}`,
                  )
                    .then(data => {
                      return data.json();
                    })
                    .then(data => {
                      let obj = {
                        givenName: data.first_name,
                        familyName: data.last_name,
                        photo: data.picture.data.url,
                        email: 'N.A',
                        type: 'FB',
                      };
                      this.props.islogged(obj);
                    });
                });
              }
            }}
            style={{width: 185, height: 35, marginTop: 15}}
            onLogoutFinished={() => console.log('danish')}
          />
        </View>
      </>
    );
  }
}
const mapDispatchToProps = dispatch => {
  return {
    islogged: user => dispatch({type: 'isloggedin', data: user}),
  };
};

export default connect(
  null,
  mapDispatchToProps,
)(Home);
