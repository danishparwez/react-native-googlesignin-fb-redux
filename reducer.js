const initialState = {
  isLoggedIn: false,
};
export default function Checkreducer(state = initialState, action) {
  switch (action.type) {
    case 'isloggedin':
      return {...state, isLoggedIn: true, data: action.data};
    case 'logOut':
      return {
        ...state,
        isLoggedIn: false,
        data: {},
      };
    default:
      return state;
  }
}
